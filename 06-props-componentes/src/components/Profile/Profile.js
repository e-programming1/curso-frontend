export default function Profile({image, alt}) {
    return (
        <img
            src={image}
            alt={alt}
        />
    )
}
