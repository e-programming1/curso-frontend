import Profile from "../Profile/Profile";

const image = "https://i.imgur.com/QIrZWGIs.jpg";
const alt = "Alan L. Hart";

export default function Gallery() {
    return (
        <section>
            <h1>Galeria:</h1>
            <Profile image={image} alt={alt} />
            <Profile image={image} alt={alt} />
            <Profile image={image} alt={alt} />
        </section>
    )
}
