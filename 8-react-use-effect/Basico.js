import React, { useEffect } from 'react';

const ExampleComponent = () => {
  useEffect(() => {
    console.log('El componente se ha montado');

    return () => {
      console.log('El componente se va a desmontar');
    };
  }, []); // Dependencias vacías, se ejecuta solo al montarse y desmontarse

  return <div>¡Hola, mundo!</div>;
};

export default ExampleComponent;
