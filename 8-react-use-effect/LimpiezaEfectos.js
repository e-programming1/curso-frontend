import React, { useState, useEffect } from 'react';

const TimerComponent = () => {
  const [count, setCount] = useState(0);

  useEffect(() => {
    const timer = setInterval(() => {
      setCount(prevCount => prevCount + 1);
    }, 1000);

    return () => {
      clearInterval(timer); // Limpieza del intervalo
    };
  }, []);

  return <div>Contador: {count}</div>;
};

export default TimerComponent;
