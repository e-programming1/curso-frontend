# curso-frontend

Material para el curso frontend en ReactJS.

Temario:

1. Introducción a React
¿Qué es React?
Ventajas de usar React (componentes, virtual DOM, unidireccionalidad).
Configuración del entorno (Create React App).
2. Componentes
Componentes funcionales vs. de clase.
Props: pasar datos a los componentes.
State: gestionar el estado interno del componente.
3. JSX
Sintaxis de JSX.
Expresiones dentro de JSX.
Elementos y componentes.
4. Manejo de eventos
Sintaxis de eventos en React.
Eventos personalizados.
Manejo de formularios.
5. Hooks
Introducción a los hooks.
useState: gestionar el estado en componentes funcionales.
useEffect: efectos secundarios, suscripciones y limpieza.
Otros hooks útiles (useContext, useReducer, etc.).
6. Enrutamiento
Introducción a React Router.
Configuración de rutas.
Enlaces y navegación.
Rutas anidadas.
7. Gestión del estado
Context API: compartir estado entre componentes.
Introducción a Redux o Zustand (opcional).
Acciones, reducers y store en Redux.
8. Fetching de datos
Llamadas a API con fetch o Axios.
Manejo de promesas y asincronía.
Mostrar datos en componentes.
9. Estilos
Métodos para estilizar componentes (CSS, CSS Modules, styled-components).
Condicionales y estilos dinámicos.
10. Pruebas
Introducción a pruebas en React (Jest, React Testing Library).
Pruebas de componentes y hooks.
11. Optimización
Memoización con React.memo, useMemo, y useCallback.
Carga perezosa de componentes.
12. Despliegue
Opciones para desplegar aplicaciones React (Netlify, Vercel, GitHub Pages).
Configuración del entorno de producción.
13. Buenas prácticas
Estructuración de proyectos.
Accesibilidad en aplicaciones React.
Uso de TypeScript (opcional, si quieres introducir tipado).
Recursos adicionales
Documentación oficial de React.
Tutoriales y cursos en línea.
