import Profile from "../Profile/Profile";

/* export function Profile() {
    return (
        <img
            src="https://i.imgur.com/QIrZWGIs.jpg"
            alt="Alan L. Hart"
        />
    );
} */

export default function Gallery() {
    return (
        <section>
            <h1>Galeria:</h1>
            <Profile />
            <Profile />
            <Profile />
        </section>
    )
}
